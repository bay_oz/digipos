<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 31/7/18
 * Time: 11:47 AM
 */

namespace Helper;


class Database
{

    public $db_file = 'tests/_data/digipos.db';

    /** @var $pdo \PDO */
    private $pdo;

    public $limit;
    public $page;
    public $table;
    public $where = '1=1';
    public $select = '*';

    /**
     * Database constructor.
     * @param array $properties
     */
    public function __construct($properties = [])
    {
        foreach ($properties as $name => $value) {
            $this->$name = $value;
        }

        $this->pdo = new \PDO("sqlite:" . $this->db_file);
    }

    public function select($select)
    {
        $this->select = $select;
        return $this;
    }

    public function where($where)
    {
        if (is_array($where))
        {
            $_where = [];
            foreach ($where as $key => $value)
                $_where[] = "($key = '$value')";

            $where = implode(' AND ', $_where);
        }
        $this->where = $where;
        return $this;
    }

    public function insert($params)
    {
        $properties = implode(',', array_keys($params));
        $keys = ":" . implode(', :', array_keys($params));
        $sql = sprintf("INSERT INTO %s (%s) VALUES (%s)", $this->table, $properties, $keys);

        $query = $this->pdo->prepare($sql);
        foreach ($params as $key => $value) {
            $query->bindValue(':' . $key, $value);
        }
        return $query->execute();
    }

    public function update($params, $where = '')
    {
        $update = '';
        if ($where) $this->where($where);

        foreach ($params as $key => $value)
        {
            $update = "$key=:$key";
        }
        $sql = sprintf("UPDATE %s set %s where %s", $this->table, $update, $this->where);

        $query = $this->pdo->prepare($sql);
        foreach ($params as $key => $value) {
            $query->bindValue(':' . $key, $value);
        }
        return $query->execute();
    }

    public function all()
    {
        $sql = sprintf("select %s from %s where %s", $this->select, $this->table, $this->where);
        $stmt = $this->pdo->query($sql);
        $return = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $return[] = $row;
        }
        return $return;
    }

    public function one()
    {
        $sql = sprintf("select %s from %s where %s", $this->select, $this->table, $this->where);
        $stmt = $this->pdo->query($sql);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function scalar()
    {
        $sql = sprintf("select %s from %s where %s", $this->select, $this->table, $this->where);
        $stmt = $this->pdo->query($sql);
        $return = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (empty($return)) return '';
        foreach ($return as $key => $value)
        {
            return $value;
        }
    }


    public function getTableList() {

        $stmt = $this->pdo->query("SELECT name FROM sqlite_master WHERE type = 'table' ORDER BY name");
        $tables = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC))
            $tables[] = $row['name'];

        return $tables;
    }
}