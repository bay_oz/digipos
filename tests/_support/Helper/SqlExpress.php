<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 31/7/18
 * Time: 11:47 AM
 */

namespace Helper;

class SqlExpress
{

    private $lastQuery = '';
    private $lastInsertId = null;
    private $error = array();
    private $db = null;
    private $schema = false;
    public $numRows = 0;
    public $hasRows = false;
    public $isConnected = false;

    public function __construct()
    {
        $this->db = sqlsrv_connect('IDACELL-PC\SQLEXPRESS', ['Database' => 'otomax']);
        $this->isConnected = (false === $this->db) ? false : true;
    }

    private function schema_prepare_value($table, $column, $value)
    {
        if (false === $this->schema || !isset($this->schema->$table) || !isset($this->schema->$table->$column)) {
            if (null === $value)
                return 'NULL';
            elseif (ctype_digit(str_replace(array('.', '-'), '', $value)) && substr_count($value, '.') < 2)
                return $value;
            else
                return "'" . addslashes(utf8_decode($value)) . "'";
        }

        $schema = $this->schema->$table->$column;
        $numeric = array(
            'int',
            'decimal',
            'money'
        );

        if (in_array($schema->TYPE_NAME, $numeric)) {
            if (null === $value || '' === $value)
                return (1 == $schema->NULLABLE) ? 'NULL' : 0;
            else
                return $value;
        } else {
            if (null === $value || empty($value))
                return (1 == $schema->NULLABLE) ? 'NULL' : "''";
        }

        return "'" . addslashes(utf8_decode($value)) . "'";
    }

    private function prepare()
    {
        $this->error = array();
        $this->lastInsertId = null;
        $this->lastQuery = '';
        $this->numRows = 0;
        $this->hasRows = false;
    }

    public function update($table, $what, $where = array())
    {
        $set = '';
        $check = '';

        foreach ($what AS $field => $value) {
            $field = trim($field);
            $value = trim($value);

            if (!empty($set)) {
                $set .= ', ';
            }
            $set .= $table . '.' . $field . ' = ';

            $set .= $this->schema_prepare_value($table, $field, $value);
        }

        foreach ($where AS $field => $value) {
            $check .= ' AND ' . $table . '.' . $field;
            if (null === $value)
                $check .= ' IS NULL';
            elseif (ctype_digit(str_replace(array('.', '-'), '', $value)) && substr_count($value, '.') < 2)
                $check .= ' = ' . $value;
            else
                $check .= " = '" . addslashes(utf8_decode($value)) . "'";

        }

        $this->query(" UPDATE " . $table . " SET " . $set . " WHERE 1 = 1 " . $check . " ", false);
    }

    public function delete($table, $where = array())
    {
        $check = '';
        foreach ($where AS $field => $value) {
            $field = trim($field);
            $value = trim($value);

            $check .= ' AND ' . $table . '.' . $field;
            if (null === $value)
                $check .= ' IS NULL';

            if (ctype_digit(str_replace(array('.', '-'), '', $value)) && substr_count($value, '.') < 2)
                $check .= ' = ' . $value;
            else
                $check .= " = '" . addslashes(utf8_decode($value)) . "'";

        }

        $this->query("DELETE FROM " . $table . " WHERE 1 = 1 " . $check . " ", false);
    }

    public function insert($table, $data)
    {
        $fields = '';
        $values = '';

        foreach ($data AS $field => $value) {
            $field = trim($field);
            $value = trim($value);

            if (!empty($fields))
                $fields .= ', ';

            if (!empty($values))
                $values .= ', ';

            $fields .= $table . '.' . $field;
            $values .= $this->schema_prepare_value($table, $field, $value);
        }

        $this->query("INSERT INTO " . $table . " ( " . $fields . " ) VALUES ( " . $values . " ) ", false);
    }

    public function one($query, $format = 'object')
    {
        $request = $this->query($query);
        $response = false;

        if (!$this->has_error()) {
            $response = ('array' == $format)
                ? sqlsrv_fetch_array($request, SQLSRV_FETCH_ASSOC)
                : sqlsrv_fetch_object($request);
        }

        return $response;
    }

    public function all($query, $format = 'object')
    {
        $request = $this->query($query);
        $response = false;

        if (!$this->has_error()) {
            if ('array' == $format) {
                while ($answer = sqlsrv_fetch_array($request, SQLSRV_FETCH_ASSOC)) {
                    $response[] = $answer;
                }
            } else {
                while ($answer = sqlsrv_fetch_object($request)) {
                    $response[] = $answer;
                }
            }
        }

        return $response;
    }

    public function last_insert_id()
    {
        if ($this->has_error() || empty($this->lastQuery))
            return false;

        if (empty($this->lastInsertId))
            $this->lastInsertId = $this->one("SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]");

        return $this->lastInsertId->SCOPE_IDENTITY;
    }

    public function get_last_id()
    {
        return $this->last_insert_id();
    }

    public function query($query, $can_get_rows = true)
    {
        // If no connection is found we try to restore it
        if (!$this->isConnected) {
            $this->isConnected = $this->db_connect();

            // If we couldn't reconnect we break out early
            if (!$this->isConnected)
                return false;
        }
        $this->prepare();
        $this->lastQuery = $query;

        $doing_query = sqlsrv_query($this->db, $query);

        if (false === $doing_query) {
            if (null != ($errors = sqlsrv_errors()))
                $this->log_error($errors);

        } else {
            $this->hasRows = true;
            $this->numRows = sqlsrv_num_rows($doing_query);
        }

        if ($can_get_rows) {
            if (sqlsrv_has_rows($doing_query))
                $this->hasRows = true;
            else
                $this->hasRows = false;
        }

        return $doing_query;
    }

    public function has_error()
    {
        if (!empty($this->error))
            return $this->error;

        return false;
    }

    public function hasError()
    {
        return $this->has_error();
    }

    public function get_last_query()
    {
        return $this->lastQuery;
    }
}