<?php

use Helper\Database;
use Helper\SqlExpress;

class DigiposCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function buy(AcceptanceTester $I)
    {
        $data = (new SqlExpress())
            ->one("SELECT * FROM transaksi", 'array');

        if (!$data)
            return;

        $packet = 'AP_BULK//!Recharge Data Bulk';
        $exception = '';

        $price = (new Database(['table' => 'package']))
            ->select('description')
            ->where(['key' => $data['kode_produk']])
            ->scalar();

        $retry = 0;

        $I->amOnPage('/');
        $I->fillField('#u.InputLogin', 'WAH6308461');
        $I->fillField('#p.InputLogin', '123321');
        $I->click('.InputLoginContainer button[type="button"]');

        $I->waitForElement('.slick-slide.slick-active.DropUp > .DropUpItem[href="/outapp/aktivasipaket"]', 60);
        $I->waitForElementNotVisible('.loaderComponent');
        $I->click('Penjualan Paket');
        $I->waitForElement('input[name=MSISDN]');
        $I->fillField('input[name=MSISDN]', $data['tujuan']);
        $I->waitForJS("return $.active == 0;", 60);
        $I->fillField('input[name=PINRS]', '000111');
        $I->selectOption('select[name=PaketDetail]', ['value' => $packet]);
        $I->waitForJS("return $.active == 0;", 60);
        $I->selectOption('select[name=Harga]', ['value' => $price]);
        //        $I->click('Kirim');
        $status = "success";

        (new Database(['table' => 'transactions']))
            ->insert([
                'phone_number' => $data['tujuan'],
                'price' => $price,
                'packet' => $packet,
                'status' => $status,
                'exception' => $exception,
                'retry' => $retry,
                'ref_code' => $data['kode'],
            ]);

    }
}
