**Installation**

- Install `php 5.6 <=`
- Enable `php-curl` at `php.ini`
- Copy file `/init/digipost.db` to `/tests/_data/digipost.db`
- Download [chromedriver](https://chromedriver.storage.googleapis.com/index.html?path=2.41/) depend on your OS
- Download [composer](https://getcomposer.org/download/1.6.5/composer.phar) 
- Download [codecept](https://codeception.com/codecept.phar)
- Download [Selenium Standalone Server](https://www.seleniumhq.org/download/)

Extract `chromedriver(.exe)`, `composer.phar` and `codecept.phar` to `/root_directory` 
then run `# php composer.phar install` for install all dependencies.

**How To Run**

open `terminal` or `cmd` and run in separate window
- Run selenium `# java -jar selenium-server-standalone-xxx.jar`
- Run Command  `# php codecept.phar run --steps`