<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/8/18
 * Time: 12:36 AM
 */

require __DIR__ . '/vendor/autoload.php';

$username = 'WAH6308461';
$password = '123321';
$digits = 2;

$login = request(
    'POST',
    'https://digipos.telkomsel.com/api/user/auth/mo',
    [
        'username' => $username,
        'password' => 'f' . str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT) . md5($password),
        'isBrowser' => 'true',
        'browser' => 'Chrome',
        'os' => 'Mac OS',
        'osVersion' => '10.13.3',
        'appVersion' => '2.4.5',
        'karyawan' => 'false',
        'browserVersion' => '67.0.3396.99',
    ]
);

print_r($login);
if ($login['httpCode'] < 300) {
//    https://digipos.telkomsel.com/api/user/profile?code=WAH6308461&_=1533762696792
    $phone = '082111508692';
    $pin = '000000';
    $purchasePackage = request(
        'POST',
        'https://digipos.telkomsel.com/api/package-activation',
        [
            'price' => 35000,
            'pin' => 3500, // *
            'msisdn' => $phone,
            'packageId' => '654',
            'rsNumber' => $login['response']['user']['rsOutlet']['rsNumber'],
            'userId' => $login['response']['user']['userId'],
            'outletId' => '5555790313', // *
        ]
    );
    print_r($purchasePackage);
}

function request($method = 'POST', $url, $params)
{
    $ch = curl_init();

    if ($method = 'GET') {
        if ($params) {
            $_params = http_build_query($params);
            $url .= ($_params ? ('&' . $_params) : '');
        }
    }
    curl_setopt_array($ch, [
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POSTFIELDS => http_build_query($params),
        CURLOPT_POST => true
    ]);

    $result = curl_exec($ch);

    curl_close($ch);

    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($result, 0, $header_size);

    return [
        'httpCode' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
        'response' => json_decode($result, true),
        'header' => json_decode($header),
    ];
}